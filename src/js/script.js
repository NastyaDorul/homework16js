

let getButton=$('.data-submit');
let buttnBgColor=getButton.css('backgroundColor');
let buttnColor=getButton.css('color');
let buttnBorderColor=getButton.css('borderColor');
let buttnAttrBgc=localStorage.setItem('bttnKeyBgc', buttnBgColor);
let buttnAttrColor=localStorage.setItem('bttnKeyColor',buttnColor);
let buttnAttrBorderColor=localStorage.setItem('bttnKeyBorderColor',buttnBorderColor);

let getThemeButton=$('.change-color');
let ThemeBgColor= getThemeButton.css('backgroundColor');
let ThemeColor= getThemeButton.css('color');
let ThemeBorderColor= getThemeButton.css('borderColor');
let ThemeAttrBgc=localStorage.setItem('themeKeyBgc', ThemeBgColor);
let ThemeAttrColor=localStorage.setItem('themeKeyColor',ThemeColor);
let ThemeAttrBorderColor=localStorage.setItem('themeKeyBorderColor',ThemeBorderColor);

let getContainer=$('.container');
let ContainerBgColor=getContainer.css('backgroundColor');
let ContainerBgc=localStorage.setItem('contKeyBgc', ContainerBgColor);


let getCheckBox=$('.a');
let CheckBoxColor=getCheckBox.css('color');
let  CheckBoxAttrColor=localStorage.setItem('CheckKeyColor',CheckBoxColor);

let getAgree=$('label');
let getlabelColor=getAgree.not('.a');
let labelColor=getlabelColor.css('color');
let labelAttrColor=localStorage.setItem('labelKeyColor',labelColor);

let notGreenRctngl=$('.not-green');

let textWithLines=$('.container .text-with-lines p');
let textColor=textWithLines.css('color');
let textBgc=textWithLines.css('backgroundColor');
let attrTextColor=localStorage.setItem('textKeyColor',textColor);
let attrBgc=localStorage.setItem('textKeyBgc',textBgc);

let jorney=$('.orange .jorney');
let jorneyColor=jorney.css('color');
let jorneyColorAttr=localStorage.setItem('jorneyKey',jorneyColor);

let login=$('.form .log-in');
let loginColor=login.css('color');
let loginColorAttr=localStorage.setItem('loginKey','loginColor');

let row=$('.right .row-item1,.row-item2');
let rowColor=row.css('color');
let rowColorAttr=localStorage.setItem('rowKey',rowColor);



$(document).ready(function(){
    var click_check=true;

    $('.change-color').click(function(){
        if(click_check) {
    getButton.css({'backgroundColor':'powderblue','color':'darkcyan','borderColor': 'white'});
    getThemeButton.css({'backgroundColor':'powderblue','color':'darkcyan','borderColor': 'darkcyan'});
    getContainer.css('backgroundColor','thistle');
    getCheckBox.css('color','white');
    getAgree.css('color','darkcyan');
    notGreenRctngl.css('display','block');
    textWithLines.css({'backgroundColor':'thistle','color':'white'});
    jorney.css('color','thistle');//addClass('newClass');
    textWithLines.addClass('classLinesLeft');
    login.css('color','white');
    row.css('color','thistle');
        }
        else {
            getButton.css({'backgroundColor':localStorage.getItem('bttnKeyBgc'),'color':localStorage.getItem('bttnKeyColor'),'borderColor':localStorage.getItem('bttnKeyBorderColor')});
            getThemeButton.css({'backgroundColor':localStorage.getItem('themeKeyBgc'),'color':localStorage.getItem('themeKeyColor'),'borderColor':localStorage.getItem('themeKeyBorderColor')});
            getContainer.css('backgroundColor', localStorage.getItem('contKeyBgc'));
            getCheckBox.css('color',localStorage.getItem('CheckKeyColor'));
            getlabelColor.css('color',localStorage.getItem('labelKeyColor'));
            notGreenRctngl.css('display','none');
            textWithLines.css({'backgroundColor':localStorage.getItem('textKeyBgc'),'color':localStorage.getItem('textKeyColor')});
            jorney.css('color',localStorage.getItem('jorneyKey'));
            textWithLines.removeClass('classLinesLeft');
            login.css('color',localStorage.getItem('loginKey'));
            row.css('color',localStorage.getItem('rowKey'));
        }
        click_check = !click_check;
    });

});







